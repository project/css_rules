
CSS Rules 5.x
-----------------------------

Description
-----------
Allows you to set rules for displaying CSS stylesheets so as to create a theme that's as flexible as possible.
For example, add a stylesheet in your bluemarine theme called node.css and add a rule to load node.css on every page
that has the path "node*" and then add an ignore rule to skip node pages with the path "node*edit", and voila! - you
have a theme you want to apply to several specific pages. 

Installation
------------
1. Extract & copy module's folder to <drupal>/sites/all/modules/.
2. Activate module.
3. Grant access rights for module's administration (optionally).
4. Configure module at admin/settings/css_rules page.
